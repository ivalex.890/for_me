#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define INPUT "input.txt"
#define OUTPUT "output.txt"
#define MAX_SIZE 100
#define MAX_DIM 10000
#define MAX_COLUMNS 20

struct node {
	char str[MAX_SIZE];			// Нужно делать realloc'ом
	//char key[MAX_SIZE];
	struct node *next;
};
typedef struct node node;

void sorting() {
    // Нужно отсортировать односвязный список
}

node *parsing(char str[], int n, node *root) {
	node *go = calloc(1, sizeof(node));
	root->next = go;
    
	int len = strlen(str);
	int t = 0;
	for (int i = 0; i < len; i++) {
		char s = str[i];
		char cl[] = {s, '\0'};
        
		if (t == n) {
			// нужно считать это значение и отсортировать, как односвязный список
		}
		
		if (s == ';') {
			strcat(go->str, ";");
			t++;
		} else if (s == '"') { 
			strcat(go->str, cl);
			i++;
    		while (str[i] != '"') { 
				cl[0] = str[i++];
				strcat(go->str, cl);
			}
		} else if (s != ' ') {
			strcat(go->str, cl);
		}
	}
	
	return go;
}

void writing(FILE *out, node *root) {
	out = fopen(OUTPUT, "w");
	while (root->next != NULL) {
		fprintf(out, "%s", root->str);
		root = root->next;
	}
	fprintf(out, "%s", root->str);
    
	fclose(out);
}

int main(void) {
	FILE *in;
	in = fopen(INPUT, "r");
	
	int n;
	fscanf(in, "%d", &n);
	char str[MAX_SIZE] = {};
	fgets(str, 2000, in);
    
	node *head = calloc(1, sizeof(node));
	node *tmp;
	
	fgets(str, 2000, in);
	tmp = parsing(str, n, head);
    
	while (fgets(str, 2000, in)) {
		tmp = parsing(str, n, tmp);
	}
	fclose(in);
    
    
	FILE *out;
	writing(out, head);
	
	return 0;
}
