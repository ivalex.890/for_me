#include <stdio.h>
#include <stdlib.h>

struct node {
	int data;
};
typedef struct node node;

void MergeSortImpl(node *list, vector<int>& buffer, int l, int r) {
	if (l < r) {
		int m = (l + r) / 2;
    
		MergeSortImpl(values, buffer, l, m);
		MergeSortImpl(values, buffer, m + 1, r);

		int k = l;
		for (int i = l, j = m + 1; i <= m || j <= r; ) {
			if (j > r || (i <= m && values[i] < values[j])) {
				buffer[k] = values[i];
				i++;
		} else {
			buffer[k] = values[j];
			j++;
		}
			k++;
		}
	
		for (int i = l; i <= r; ++i) {
			values[i] = buffer[i];
		}
	}
}

void MergeSort(node *list, int len) {
	if (len > 0) {
		vector<int> buffer(values.size());
		MergeSortImpl(list, buffer, 0, len - 1);
	}
}

int main(void) {

	return 0;
}
