#include <stdio.h>

void print_arr(int arr[], int len) {
	for (int i = 0; i < len; i++) {
		printf("%d ", arr[i]);
	}
}

void swap(int *a, int *b) {
	int tmp = *b;
	*b = *a;
	*a = tmp;
}

int comp(const void *a, const void *b) {
	return (*(int *)a - *(int *)b);    
}

void sort_t(int arr[], int len) {
	for (int i = 0; i < len; i++) {
		for(int j = 0; j < len - i - 1; j++) {
			if (comp(&arr[j], &arr[j + 1]) > 0) {
				swap(&arr[j], &arr[j + 1]);
			}
		}
	}
}

void sort(int arr[], int len) {
	for (int i = 0; i < len; i++) {
		for (int j = 0; j < len - i - 1; j++) {
			if (arr[j] > arr[j + 1]) {
				swap(&arr[j], &arr[j + 1]);
			}
		}
	}
}

int main(void) {
	int arr[] = {-1, 6, -2, 0, 1};
	
	sort_t(arr, 5);
	print_arr(arr, 5);
    
	return 0;
}
