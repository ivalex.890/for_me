#include <stdio.h>

struct NODE {
    bool is_leaf;
    union {
        struct {
            struct NODE* left;
            struct NODE* right;
        } internal;
        double data;
    } info;
};

int main(void) {
	union DATA data;
	scanf("%s", data.data);
	printf("%d", data.msg1.id);
	return 0;
}
