#include <stdio.h>
#include <stdlib.h>

struct node {
	int data;
	struct node *next;
};
typedef struct node node;

void print_list(node *root) {
	while (root != NULL) {
		printf("%d ", root->data);
		root = root->next;
	}
}

node *add_to_list(node *root, int data) {
	if (root == NULL) {
		root = calloc(1, sizeof(node));
		root->data = data;
		return root;
	}
	node *tmp = root;
	while (root->next != NULL) {
		root = root->next;
	}
	
	root->next = calloc(1, sizeof(node));
	root = root->next;
	root->data = data;
    
	return tmp;
}

node *cut_elements(node *root, int first, int len) {
	if (root == NULL || len == 0 || first < 0) {
		return root;
	}
	
	if (first == 0) {
		for (int i = 0; i < len; i++) {
			if (root == NULL) {
				return NULL;
			} else {
				node *prev = root;
				root = root->next;
				free(prev);
			}
		}
		
		return root;
	}

	node *head = root;

	for (int i = 0; i < first - 1; i++) {
		root = root->next;
        
		if (root == NULL) {
			return head;
		}
	}

	node *begin = root;
	root = root->next;
	for (int i = 0; i < len; i++) {
		if (root == NULL) {
			begin->next = NULL;
			return head;	
		}
		node *prev = root;
		root = root->next;
		free(prev);
	}
	begin->next = root;

	return head;
}

int main(void) {
	const int SIZE = 10;
	int arr[] = {10, 20, 30, 40, 50, 60, 70, 80, 90, 100};
    
	node *head = NULL;
	for (int i = 0; i < SIZE; i++) {
		head = add_to_list(head, arr[i]);
	}
	print_list(head);
    
	printf("\n");
    
	head = cut_elements(head, 1, 9);
	print_list(head);
    
	return 0;
}
