/*Простое натуральное число называется гиперпростым, если любое число, получающееся из него откидыванием нескольких последних цифр, тоже является простым. Например, число 7331 – гиперпростое, т.к. и оно само, и числа 733, 73, 7 являются простыми. Найдите все N-значные гиперпростые числа.

Входные данные: единственное целое число N (1 ≤ N ≤ 9).

Выходные данные: возрастающая последовательность целых чисел через пробел – ответ задачи. 
2 3 5 7
*/
#include <stdio.h>
#include <math.h>

#define DEC 10
#define LEN 6
#define LEN_MAIN 4

int proverka(int number) {
    char flag = 1;
    int sq = sqrt(number) + 1;
    
    for (int i = 2; i < sq; i++) {
        if (!(number % i)) {
            flag = 0;
            break;
        }
    }
    
    if (flag) {
        return 1;
    }else {
        return 0;
    }
    
}

void making(int n, int count, int number) {
    
    if (count == n) {
        
        if (proverka(number)) {
            printf("%d ", number);
        }
        
    }else {
        if (proverka(number)) {
            int num[LEN] = {1, 2, 3, 5, 7, 9};
        
            for (int i = 0; i < LEN; i++) {
                making(n, count + 1, number * DEC + num[i]);
            }
        }else {
            return;
        }
    }
        
}


int main(void) {
    int arr[LEN_MAIN] = {2, 3, 5, 7};
    int n = 0;
    
    scanf("%d", &n);
    for (int i = 0; i < LEN_MAIN; i++) {
        making(n, 1, arr[i]);
    }
    
    return 0;
    
}
