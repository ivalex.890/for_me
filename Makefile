compile:
	gcc -m32 -std=c99 -c main.c -o main.o
	nasm -f elf32 countof.asm -o countof.o
	gcc -m32 main.o countof.o -o main

run:
	./main

clean:
	@rm *.o main 2>/dev/null || true
