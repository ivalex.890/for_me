#include <stdio.h>
#include <stdlib.h>

#define INPUT "input.txt"
#define OUTPUT "output.txt"

#define MAX_SIZE 100001

struct node {
	int data;
};
typedef struct node node;

void write(FILE *out, node **root, int len) {
	for (int i = 0; i < len; i++) {
		fprintf(out, "%d ", root[i]->data); 
	}
	
	return ;
}

void swap(node *root1, node *root2) {
	int tmp = root1->data;
	root1->data = root2->data;
	root2->data = tmp;
}

int Partition(node **root, int l, int r) {
	int x = root[r]->data;
	int less = l;

	for (int i = l; i < r; ++i) {
		if (root[i]->data <= x) {
			swap(root[i], root[less]);
			++less;
		}
	}
	swap(root[less], root[r]);
    
	return less;
}

void quick_sort(node **root, int l, int r) {
	if (l < r) {
		int q = Partition(root, l, r);
		quick_sort(root, l, q - 1);
		quick_sort(root, q + 1, r);
	}
}

node **sort(node **root, int len) {
	if (len > 1) {
		quick_sort(root, 0, len - 1);
	}
	
	return root;
}

void clear_memory(node **root, int len) {
	for (int i = 0; i < len; i++) {
		free(root[i]);
	}

	free(root);
}

int main(void) {
	FILE *in;
	in = fopen(INPUT, "r");
    
	int data;
	int len = 0;
	node **head = calloc(MAX_SIZE, sizeof(node *));
	int i = 0;
	while (fscanf(in, "%d", &data) != EOF) {
		head[i] = (node *) calloc(1, sizeof(node));
		head[i]->data = data;
		len++;
		i++;
	}
	fclose(in);
	
	sort(head, len);


	FILE *out;
	out = fopen(OUTPUT, "w");

	write(out, head, len);
	fclose(out);
    
	clear_memory(head, len);

	return 0;
}
