#include <stdio.h>

#define MAX 80

void drawing(long long int n, char flag) {
    
    // Variable for counting spaces
    long long int k = 1L;  
    // Variable for making an space before diomand
    long long int a = 3L;
    
    // if draw low part needs these settings
    if (!flag) {
        k = n;
        n -= 1;
        a += 1;
    }

    for (long long int i = 0L; i < n + 1; i++) {
        for (long long int j = 0L; j < n - k + a; j++) {
            printf(" ");
        }
        
        printf("*");
        
        for (long long int j = 0L; j < 2 * (k) - 3; j++) {
            printf(" ");
        }
        
        if (flag) {
            if (k > 1) {
                printf("*");
            }
        }else {
            if (k > 1) {
                printf("*");
            }
        }
        
        // Changing parameters for for's
        if (flag) {
            k++;
        }else {
            k--;
        }
        
        printf("\n");

    }
    
}

int main() {
    long long int n = 0;
    long long int h = 0;
    
    // Input parameters
    printf("Enter height of your diamand: ");
    scanf("%lld", &h);
    
    // Changing some mistakes with uncorrecting height
    while ( (!(h % 2) || h < 0) && h <= MAX) {
        printf("Sorry, it's not correct height.\n");
        printf("Please, write again: ");
        scanf("%lld", &h);
    }
    printf("\n");
    
    // Printing diamand
    n = h / 2;
    
    drawing(n, 1);  // draw upper part
    drawing(n, 0);  // draw low part
    
    printf("\n");
         
    return 0;
    
}
