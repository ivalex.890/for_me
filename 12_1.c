#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_SIZE 2003
#define DIM 26

struct node {
	char str[MAX_SIZE];
	struct node *arr[DIM];
};
typedef struct node node;

void clean(node *root) {
	for (int i = 0; i < DIM; i++) {
		if(root->arr[i] != NULL) {
			clean(root->arr[i]);
		}
	}
	
	free(root);
}

void add(node *root, char *str, long long *res) {
	node *go = root;
	int num = str[0] - 'a';
	if (go->arr[num] == NULL) {
		go->arr[num] = calloc(1, sizeof(node));
		strcat(go->arr[num]->str, str);
		(*res)++;
	} else if (go->arr[num]->str[0] == str[0]) {
		strcat(go->arr[num]->str, str);
		(*res)++;
	}
	for (int i = 0; i < DIM; i++) {
		if (i != num && go->arr[i] != NULL) {
			strcat(go->arr[i]->str, str);
			(*res)++;
		}
	}
}

void mk_tree(node *root, char str[], long long *res) {
	int len = strlen(str);
	for (int i = 0; i < len; i++) {   
		char s[2] = {str[i], '\0'};
		add(root, s, res);
	}
}


int main(void) {
	char str[MAX_SIZE] = {};
	scanf("%s", str);
	
	long long res = 1;
	node *head = (node *) calloc(1, sizeof(node));
	if (head != NULL) {
		mk_tree(head, str, &res);
	}
	
	printf("%lld", res);
	
	//free memory
	clean(head);
    
	return 0;
}
