#include <stdio.h>

int main(void) {
	int arr1[] =  {2, 8, 8};
	int arr2[] = {3, 4, 5, 5, 10};
	
	int len1 = 3;
	int len2 = 5;
	int i = 0, j = 0, cnt = 0;
	int arr[8] = {};
	while (i < len1 && j < len2) {
		if (arr1[i] > arr2[j]) {
			arr[cnt++] = arr2[j++];
		} else {
			arr[cnt++] = arr1[i++];
		}
	}
	if (i < len1) {
		for (int k = i; k < len1; k++) {
			arr[cnt++] = arr1[k];
		}
	} else if (j < len2) {
		for (int k = j; k < len1; k++) {
			arr[cnt++] = arr2[k];
		}
	}
	
	
	for (int i = 0; i < cnt; i++) {
		printf("%d ", arr[i]);
	}
	
	return 0;
}
