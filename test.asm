%include "io.inc"

section .data
	a dw 0x100, 0x500
section .text
	global CMAIN
CMAIN:
	movsx eax, word[a]
	js .end
	PRINT_HEX 4, eax
	add ah, 0xAC
	PRINT_HEX 4, eax
	js.end
.end:
	xor eax, eax
	ret
	
