extern qsort
extern printf

%define arg1 ebp+8
%define arg2 ebp+12

%macro print 1-*
	%rep %0
		%rotate -1
			push dword %1
	%endrep
	call printf
	mov ecx, 4
	mov eax, %0
	mul ecx
	add esp, eax
%endmacro

section .data
	fmt db `Hello!\n`, 0
	f db `%d %d\n`, 0
	arr dd 1, 4, -1, 2
	a db `%d %d %d %d\n`, 0
section .text
	global main
main:
	mov eax, 1131
	mov ecx, 1313
	print f, eax, ecx
	
	push ff
	push dword 4
	push dword 4
	push arr
	call qsort
	add esp, 16
	
	print a, dword[arr], dword[arr+4], dword[arr+8], dword[arr+12]
	
	xor eax, eax
	ret
	
ff:
	push ebp
	mov ebp, esp
	push ebx
	
	mov eax, dword[arg1]
	mov ecx, dword[arg2]
	
	mov ecx, dword[ecx]
	sub dword[eax], ecx
	
	pop ebx
	mov esp, ebp
	pop ebp
	ret
	
