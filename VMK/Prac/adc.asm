extern printf

; %macro print 1-*
; 	%rep %0
; 		%rotate -1
; 			push %1
; 	%endrep
; 	call printf
; 	mov eax, 4
; 	mov ecx, %0
; 	mul ecx
; 	add esp, eax
; %endmacro

section .data
	d dq 1
	fmt db `%lld\n`, 0
	
section .text
	global main
main:
	add dword[d], 1
	adc dword[d + 4], 0
	
	push dword[d + 4]
	push dword[d]
	push fmt
	call printf
	add esp, 12
	
	xor eax, eax
	ret
