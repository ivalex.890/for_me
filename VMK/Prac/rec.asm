extern printf

%define arg1 ebp+8

section .data
	fmt db `%d\n`, 0
	k dd 5
	
section .text
	global main
main:
	push dword[k]
	call f
	add esp, 4
	
	xor eax, eax
	ret
	
f:
	push ebp
	mov ebp, esp
	
	mov eax, dword[arg1]
	
	cmp eax, 0
	je .L1
	
	push eax
	
	push eax
	push fmt
	call printf
	add esp, 8
	
	pop eax
	dec eax
	push eax
	call f
	add esp, 4
	
.L1:
	mov esp, ebp
	pop ebp
	ret
