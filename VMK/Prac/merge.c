#include <stdio.h>
#include <stdlib.h>

typedef struct node {
	int a;
	struct node *next;
} node;

node *add(node *root, int a) {
	node *head = malloc(1 * sizeof(node));
	if (!head) {
		fprintf(stderr, "[ERROR] with memory\n");
		exit(1);
	}
	
	if (!root) {
		head->a = a;
		head->next = NULL;
		return head;
	}
	
	head->a = a;
	head->next = root;
	
	return head;
}

void print(node *root) {
	if (root != NULL) {
		printf("%d ", root->a);
		fflush(stdout);
	
		print(root->next);
	}
}

node *split(node *a, node *b) {
	node* result = NULL;
	
	if (a->a >= b->a) {
		result = b;
		b = b->next;
	} else {
		result = a;
		a = a->next;
	}
	node *copy = result;
	
	while (a != NULL || b != NULL) {
		if (b == NULL) {
			result->next = a;
			result = result->next;
			a = a->next;
			continue;
		}
		
		if (a == NULL) {
			result->next = b;
			result = result->next;
			b = b->next;
			continue;
		} 
		
		if (a->a >= b->a) {
			result->next = b;
			b = b->next;
		} else {
			result->next = a;
			a = a->next;
		}
		result = result->next;
		
	}
	
	return copy;
}

node *slice(node *root, node **a, node **b) {
	node *fast = root->next;
	node *slow = root;
	
	while (fast->next != NULL) {
		fast = fast->next;
		if (fast->next != NULL) {
			fast = fast->next;
			slow = slow->next;
		}
	}
	
	*b = slow->next;
	*a = root;
	slow->next = NULL;
	
	return slow;
}

void merge(node **root) {
	node *head = *root;
	if (head == NULL || head->next == NULL) {
		return;
	}
	
	node *a, *b;
	slice(head, &a, &b);
	
	merge(&a);
	merge(&b);
	
	*root = split(a, b);
}

int main(void) {
	node *list = NULL;
	list = add(list, 1234);
	list = add(list, 7);
	list = add(list, 2);
	list = add(list, 4);
	list = add(list, 131313);
	list = add(list, 1656);
	
	merge(&list);
	print(list);
	return 0;
}
