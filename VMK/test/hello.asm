global main: function

extern puts                                             ; near
extern _GLOBAL_OFFSET_TABLE_                            ; byte


SECTION .text   ;section number 1, code


SECTION .data                         ; section number 2, data


SECTION .bss                           ; section number 3, bss


SECTION .rodata.str1.1                ; section number 4, const

.LC0:                                                   ; byte
        db 48H, 65H, 6CH, 6CH, 6FH, 20H, 77H, 6FH       ; 0000 _ Hello wo
        db 72H, 6CH, 64H, 00H                           ; 0008 _ rld.


SECTION .text.startup ;align=16 execute                  ; section number 5, code

main:   ; Function begin
        sub     esp, 8                                  ; 0000 _ 48: 83. EC, 08
        lea     edi, [rel .LC0]                         ; 0004 _ 48: 8D. 3D, 00000000(rel)
        call    puts                                    ; 000B _ E8, 00000000(PLT r)
        xor     eax, eax                                ; 0010 _ 31. C0
        add     esp, 8                                  ; 0012 _ 48: 83. C4, 08
        ret                                             ; 0016 _ C3
; main End of function


