#include <stdio.h>

void f(int i) {
	int a[3] = {1, 2, 3};
		printf("%x\n", a[i]);
}

int main() {
	for (int i = 0; i < 7; i++) {
		f(i);
	}
	return 0;
}
