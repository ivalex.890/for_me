struct seq {
	signed char c;
	short s;
	struct seq *next;
};

int f (struct seq *q) {
	if (!q) 
		return 42;
	return (f(q->next) % 2) ? q->s : q-> c;
}
