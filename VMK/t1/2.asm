extern printf


section .text
	global main
main:
; 	p && q && (p[2] += *q--);
    
	push	ebp
	mov	ebp, esp
	sub	esp, 16
	cmp	dword[p], 0		; p
	je	.L1
	
	cmp	dword[q], 0		; q
	je	.L1
	
	lea eax, [q]
	lea	edx, [eax - 2]
	mov	dword[q], edx
	movzx	eax, word[eax]
	lea edx, [p + 8]
	mov	ecx, dword[edx]
	movsx	edx, ax
	lea eax, [p]
	add	eax, 8
	add	edx, ecx
	mov	dword[eax], edx
	mov	eax, dword[eax]
	jmp .L2
	
.L1:
	mov	eax, 0
.L2:
	xor eax, eax
	ret
	
