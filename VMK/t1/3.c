struct seq {
	unsigned char c;
	struct seq *next;
	unsigned short s;
};

int f (struct seq *q) {
	if (!q) 
		return 42;
	return q->s + (f(q->next) >> q->c);
}
