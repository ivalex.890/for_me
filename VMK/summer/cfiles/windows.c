#include <ncurses.h>

int main() {
	start_color();
	noecho();
	cbreak();
	init_pair(1, COLOR_YELLOW, COLOR_BLUE);
	init_pair(2, COLOR_WHITE, COLOR_YELLOW);
    
	WINDOW *win = initscr();
	refresh();
	box(win, 0, 0);
	wbkgd(stdscr, COLOR_PAIR(1));
	wrefresh(win);
    
	WINDOW *sub = subwin(win, 4, 20, 0, 0);
	box(sub, 0, 0);
	wbkgd(sub, COLOR_PAIR(2));
    refresh();
	touchwin(sub);
	wrefresh(sub);
    
	WINDOW *subb = subwin(sub, 2, 10, 0, 0);
	refresh();
	box(subb, 0, 0);
	wbkgd(subb, COLOR_PAIR(2));
	touchwin(sub);
	wrefresh(sub);
    
	getch();
	refresh();
	endwin();
// 	
	return 0;
}
