#include <curses.h>

/*enum {key_escape = 27};*/

static char print_scr(FILE *file, int row) 
{
	char flag = 1;
	
	char c;
	for (int i = 0; i < row;) {
		if ((c = fgetc(file)) != EOF) {
			addch(c);
			if (c == '\n') {
				i++;
			}
		} else {
			flag = !flag;
			break;
		}
	}
	refresh();
	
	return flag;
}

static void offset_file(FILE *file, int target)
{
	int step = 2;
	long pos = ftell(file);
	
	int c;
	if (target) {
		while ((c = fgetc(file)) != '\n')
		{}
	} else {
		do {
			pos -= step;
			if (pos < 0) {
				fseek(file, 0, SEEK_SET);
				break;
			}
			fseek(file, pos, SEEK_SET);
			pos++;
		} while ((c = fgetc(file)) != '\n');
	}
}


int main() 
{
	initscr();
	cbreak();
	noecho();
	curs_set(0);			/* delete a cursor */
	keypad(stdscr, true);
	
	int work_bw = !has_colors();
	if (!work_bw) {
		start_color();
	}
	
	int row, col;
	getmaxyx(stdscr, row, col);
	
	/* open any file */
	FILE *file = fopen("g.c", "r");
	
	long int mark = 0;
	char flag = print_scr(file, row);
	fseek(file, mark, SEEK_SET);
	
	int c;
	while ((c = getch()) != '\n') {
		switch (c) {
			case KEY_DOWN:
				if (flag) {
					offset_file(file, 1);
				}
				mark = ftell(file);
				move(0, 0);
				flag = print_scr(file, row);
				fseek(file, mark, SEEK_SET);
				break;
			case KEY_UP:
				offset_file(file, 0);
				mark = ftell(file);
				move(0, 0);
				print_scr(file, row);
				fseek(file, mark, SEEK_SET);
				break;
		}
	}
	fclose(file);
	getch();
	endwin();

	return 0;
}
