#include <stdio.h>
#include <stdlib.h>

typedef struct node {
	int data;
	struct node *next;
} node;

struct node *init(void) {
	struct node *ptr;
	ptr = malloc(sizeof(struct node));
	
	if (!ptr) {
		perror("void add");
		exit(1);
	}
	
	return ptr;
}
/*
void add(struct node **head, int data) {
	if (!(*head)) {
		*head = init();
		(*head)->data = data;
		(*head)->next = NULL;
	} else {
		
		struct node *ptr = *head;
		while (ptr->next) {
			ptr = ptr->next;
		}
		
		node *tmp = malloc(sizeof(struct node));
		ptr->next = tmp;
		tmp->data = data;
		tmp->next = NULL;
	}
}*/
/*
node *add(struct node *head, int data) {
	struct node *tmp = malloc(sizeof(node));
	if (!tmp) {
		perror("void add");
		exit(1);
	}
	
	head->next = tmp;
	tmp->data = data;
	tmp->next = NULL;
	
	return tmp;
}*/

void add(struct node **head, int data) {
	struct node *ptr = malloc(sizeof(struct node));
	if (!ptr) {
		perror("void add");
		exit(1);
	}
		
	if (!(*head)) {
		ptr->next = NULL;
	} else {
		ptr->next = *head;
	}
	
	ptr->data = data;
	*head = ptr;
}

void del(node **p) {
	while(*p) {
		if ((*p)->data < 0) {
			struct node *tmp = *p;
			*p = (*p)->next;
			free(*tmp);
		} else {
			p = &(*p)->next;
	}
}
/*
1			2
дата		дата
адрес		адрес*/

int main() {
	int arr[] = {1, -3, 2, 0, 2};
	
	struct node *root = NULL;
	for (int i = 0; i < 3; i++) {
		add(&root, arr[i]);
	}
	
	struct node **p = root->next;
	*p = (*p)->next;
	
	struct node *tmp = root;
	while (tmp) {
		printf("%d ", tmp->data);
		tmp = tmp->next;
	}
	return 0;
}
