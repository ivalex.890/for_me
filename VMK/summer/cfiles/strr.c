#include <stdio.h>
#include <stdarg.h>

int sum(int a, ...) {
	va_list vl;
	
	int s = a;
	int k;
	va_start(vl, a);
	while((k = va_arg(vl, int)) != 0) {
		s += k;
	}
	va_end(vl);
	
	return s;
}

void print(const char *s, ...) {
	va_list vl;
	const char *p;
	
	va_start(vl, s);
	for (p = s; p != NULL; p = va_arg(vl, const char *)) {
		int n = va_arg(vl, int);
		for (int i = 0; i < n; i++) {
			printf("%s\n", p);
		}
	}
	va_end(vl);
}

int main() {
	int a = sum(3, 4, 5, 6, 0);
	printf("%d\n", a);
	
	
	print("Fuck", 2, "hui", 3, 0);
	
	return 0;
}
