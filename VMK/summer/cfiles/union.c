#include <stdio.h>

union split_int {
	int integer;
	unsigned char bytes[sizeof(int)];
};

enum colors {
	red,
	blue
};

int main() {
	union split_int s;
	scanf("%d", &s.integer);
	for (int i = 0; i < sizeof(int); i++) {
		printf("byte #%d is %-2d\n", i, s.bytes[i]);
	}
	
	enum colors col;
	col = red;
	col = 2;
	
	return 0;
}
