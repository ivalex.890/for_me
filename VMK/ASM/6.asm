extern scanf, printf
%include "io.inc"

section .rodata
	fmt_f db "%lf", 0
	
section .data
	two dq 2.0
	
section .bss
	r resq 1
	h resq 1
	res resq 1
	
section .text
	global main
main:
	sub esp, 8
	push r
	push fmt_f
	call scanf
	add esp, 16
	
	sub esp, 8
	push h
	push fmt_f
	call scanf
	add esp, 16
	
	mov eax, r
	mov edx, res
	call circle
	
	sub esp, 4
	push dword[res+4]
	push dword[res]
	push fmt_f
	call printf
	add esp, 16
	PRINT_CHAR `\n`
	
	push h
	push r
	push res
	call fun
	
	sub esp, 4
	push dword[res+4]
	push dword[res]
	push fmt_f
	call printf
	add esp, 16
	
	xor eax, eax
	ret
	
circle:
	push ebp
	mov ebp, esp
	
	finit
	fldpi
	fld qword[two]
	fmulp
	
	fld qword[eax]
	fmulp
	fld qword[eax]
	fmulp
	fstp qword[edx]
	
	mov esp, ebp
	pop ebp
	ret
	
fun:
	push ebp
	mov ebp, esp
	push ebx

	mov eax, dword[ebp+16]
	mov ebx, dword[ebp+12]
	mov edx, dword[ebp+8]
	
	finit
	fldpi
	fld qword[two]
	fmulp
	fld qword[eax]
	fmulp
	fld qword[ebx]
	fmulp
	fld qword[edx]
	faddp
	fstp qword[edx]
	
	pop ebx
	
	mov esp, ebp
	pop ebp
	
	ret 12
