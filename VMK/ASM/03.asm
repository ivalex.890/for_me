extern io_get_dec
%include "io.inc"

N EQU 1000

section .bss
	arr: resd N
	
section .text
	global CMAIN
CMAIN:
	;call io_get_dec
		ecx = 500
.loop 	lea eax, [arr + 2 * ecx]
		mov eax, [eax]
		call io_print_dec
		loopd .loop
	
	xor eax, eax
	ret
	
