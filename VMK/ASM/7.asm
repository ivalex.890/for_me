extern fopen, fclose, fscanf, fprintf
extern malloc, free
extern strcpy

%include "io.inc"

EOF EQU 0
NULL EQU 0
SIZE_STRING EQU 80
SIZE_STRUCT EQU SIZE_STRING+12

section .rodata
	in_f db "input.txt", 0
	fmt_in db "r", 0
	fmt_d db "%d", 0
	fmt_str db "%s", 0
	
section .bss
	file resd 1
	a resd 1
	b resd 1
	strr resb 81
	head resd 1

section .text
	global main
main:
	sub esp, 8
	push fmt_in
	push in_f
	call fopen
	add esp, 16
	mov dword[file], eax
	
.L1:
	sub esp, 4
	push a
	push fmt_d
	push dword[file]
	call fscanf
	add esp, 16
	
	cmp eax, EOF
	jle .L2
	
	PRINT_DEC 4, [a]
	PRINT_CHAR `\n`
	
	sub esp, 4
	push b
	push fmt_d
	push dword[file]
	call fscanf
	add esp, 16
	
	PRINT_DEC 4, [b]
	PRINT_CHAR `\n`
	sub esp, 4
	push strr
	push fmt_str
	push dword[file]
	call fscanf
	add esp, 16
	
	PRINT_STRING [strr]
	PRINT_CHAR `\n`
	
	cmp eax, EOF
	jle .L2
	
	cmp dword[head], NULL
	jne .L3
	
	sub esp, 12
	push SIZE_STRUCT
	call malloc
	add esp, 16
	mov dword[head], eax
	
	mov ecx, dword[a]
	mov dword[eax], ecx
	mov ecx, dword[b]
	mov dword[eax+4], ecx
	mov dword[eax+8], NULL
	sub esp, 8
	push strr
	lea ecx, [eax+12]
	push ecx
	call strcpy
	add esp, 16
	
	PRINT_CHAR 'a'
	jmp .L1
.L3:
	mov ebx, dword[a]
	mov eax, dword[head]
	
.L6
	cmp dword[eax], ebx
	jb .L4
	
	mov ebx, dword[b]
	cmp dword[eax+4], ebx
	jl .L4
	
	
.L4:
	mov eax, dword[eax+8]
	jmp .L6
.L5:
	mov eax
	sub esp, 12
	push SIZE_STRUCT
	call malloc
	add esp, 16
	
	mov ecx, dword[a]
	mov [eax], ecx
	mov ecx, dword[b]
	mov [eax+4], ecx
	
	push eax
	sub esp, 8
	push strr
	lea ecx, [eax+12]
	push ecx
	call strcpy
	add esp, 16
	pop eax
	mov ecx, dword[head]
	mov [eax+8], ecx
	mov dword[head], eax
	jmp near .L1
.L2:
	
	
	xor eax, eax
	ret
