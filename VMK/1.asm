%include "io.inc"

section .text
	global main
main:
	mov al, 240
	add al, 20
	pushf 
	mov eax, dword[esp+4]
	PRINT_HEX 2, ax
	mov ebx, 1
	shl ebx, 7
	test eax, ebx
	jz .exit
	PRINT_CHAR 'a'
.exit:
	pop eax
	xor eax, eax
	ret
