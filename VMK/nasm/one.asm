%include "io.inc"

section .data
    str: db 'He\n', 0
    
section .text

global _start
_start:
    PRINT_STRING [str]
    MOV EAX, 0
    RET
