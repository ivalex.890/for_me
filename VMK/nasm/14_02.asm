%include "io.inc"

section .bss
	a: resd 1

section .data
	b: dd 1
	c: dd -10
	d: dd 3

section .text
	global CMAIN
CMAIN:
	mov eax, dword[c]
	mul dword [d]
	PRINT_DEC 4, eax
	add dword [b], eax
	mov eax, dword [b]
	mov dword [a], eax
	PRINT_DEC 4, [a]
	mov eax, 0
	ret
	
