%include "io.inc"

section .data
	n: dd 15
section .text
global CMAIN
CMAIN:
	mov ecx, 0;
.l1:
	PRINT_DEC 4, eax
	add ecx, 1
	cmp ecx, n
	jle .l1
	mov eax, 0
	ret
