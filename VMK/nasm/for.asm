%include "io.inc"

section .bss
	i: resd 1

section .text
	global CMAIN
CMAIN:
	mov dword[i], 32
	cmp dword[i], 0		; for (int i = 32; i > 0; i--)
	jle .end			; if i <= 0 -> end of for
.l1:
	PRINT_DEC 4, i
	PRINT_CHAR `\n`
	sub dword[i], 1		; i--
	jg .l1
.end:
	mov eax, 0
	ret
