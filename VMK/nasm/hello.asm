%include 'io.inc'

section .data
	str: db "Hello World`\n`", 0
	hex_int: dd 0xFFFFFFFF
	
section .text ; (2)

global CMAIN ; (4)
CMAIN:
	;PRINT_UDEC 2, uint		; print first bites size from unighted data(мы принимаем, что
	; безнакое типо
	;GET_DEC 2, DI	; get from keyboard decimals
	; GET_DEC size, data
	; GET_DEC size, data
	; _HEX size, data
	;PRINT_STRING data_1	; print string
	GET_HEX 2, hex_int
	;PRINT_CHAR	'a'	; print one literal
	;PRINT_HEX 4, hex_int	; print first bites "4" from data
	PRINT_HEX 2, hex_int
	PRINT_CHAR 10
	MOV EAX, 0
	RET
