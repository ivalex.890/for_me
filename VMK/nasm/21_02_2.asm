%include "io.inc"

section .text
global CMAIN
CMAIN:
	mov al, 200
	mov bl, 200
	movzx ax, al
	movzx bx, bl
	add ax, bx
	; add al, bl
	;это было решения проблемы переполнения суммы с расширением
	PRINT_UDEC 2, ax	; 144 = 400 % 256))) = 144
	
	; решение без расширения
	;mov ah, 0
	;add al, bl
	;adc ah, 0
	mov eax, 0
	ret
