%include "io.inc"

section .data
	a: dd -145
	b: dd 8

section .text
	global CMAIN
CMAIN:
	mov eax, dword [a]
	cdq
	idiv dword [b]
	PRINT_HEX 4, edx
	PRINT_CHAR `\n`
	PRINT_DEC 4, eax
	mov eax, 0
	ret
