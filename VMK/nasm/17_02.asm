; Code with jump
%include "io.inc"

section .bss
	a: resd 1
	b: resd 1
	c: resd 1

section .text
	global CMAIN
CMAIN:
	mov dword[a], 1		; numbers just for example
	mov dword[b], 2
	mov dword[c], 5
	mov eax, dword[a]
	mov ebx, dword[b]
	cmp ebx, ebx
	jg .l1				; if (a > b) -> print(that a > b), else "we" don't jump
	cmp dword[c], 0		; compare c with 0, if c != 0 -> print 
	jnz .l2
	jmp .l3
.end:
	mov eax, 0
	ret
	
.l1:
	PRINT_STRING "a is greter"
	PRINT_CHAR `\n`
	jmp .end
.l2:
	PRINT_STRING "a not greater"
	PRINT_CHAR `\n`
	jmp .end
.l3:
	PRINT_STRING "else"
	PRINT_CHAR `\n`
	




; code with pointers

;section .bss 
;	p resd 1
;	
;section .text
;	global CMAIN
;CMAIN:
;	MOV EAX, dword[p]
;	MOV EAX, dword[EAX]
;	MOV EAX, dword[EAX]
;	ADD EAX, 1
;	PRINT_DEC 4, EAX
;	MOV eax, 0
;	RET
