%include "io.inc"
; Print sum(x[n]) x[n] = массив
section .bss
	n: resd 1

section .text
global CMAIN
CMAIN:
	mov ecx, 0
	GET_DEC 4, eax
.l1:
	cmp ecx, eax
	jge .end
	GET_DEC 4, ebx
	add dword[n], ebx
	inc ecx
	jmp .l1
.end:
	PRINT_DEC 4, [n]
	xor eax, eax
	ret
