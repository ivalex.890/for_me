%include "io.inc"
global CMAIN

section .bss
	z: resd 1

section .data
	h: dw -5

	matrix: db 1, 2, 3

section .text
CMAIN:
	inc word[h]			; 	h++
	sub [z], dword 2	; 	z -= 2
	; or sub dword [z], 2	assembler undersand it too

	mov ebx, dword 2
	lea eax, [1 + ebx*2];	load effective address. Get access to compute 
	; one number(address) in formula
	; effective address [constant + eax/../esp + eax./.../ * (1, 2, 4, 8)]
	; often use for computing integers!)

	; printing
	PRINT_HEX 4, eax
	PRINT_CHAR `\n`	
	PRINT_HEX 2, [h]	;	print 2 bytes of h-varible
	PRINT_CHAR ' '		; 	print space betwen two numbers
	PRINT_HEX 4, z		;	print 4 bytes of z-varible
	PRINT_CHAR `\n`		; 	print enter after numbers(assembler get ready undersand it)
	MOV EAX, 0			;	the end of program
	RET
