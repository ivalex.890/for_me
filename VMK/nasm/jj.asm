%include "io.inc"
; for(int i =0; i < n; i++)
; printf("%d", i);
section .data
	n: dd 15
; Макрос N EAX 10
section .text
global CMAIN
CMAIN:
	mov ecx, 0;
.l1:
	cmp ecx, dword[n]
	jge .end
	PRINT_DEC 4, ecx
	add ecx, 1
	jmp .l1

.end:
	mov eax, 0
	ret
