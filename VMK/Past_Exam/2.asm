extern printf
extern scanf

%define ARG1 ebp+8
%define ARG2 ebp+12
%define ARG3 ebp+16
%define ARG4 ebp+20

section .bss
    n resd 1
    
section .rodata
    fmt db `%c -> %c\n`, 0
    fmt_d db "%d", 0
    
section .data
    a dd 'A'
    b dd 'B'
    c dd 'C'
    
section .text
    global main
main:
    push n
    push fmt_d
    call scanf
    add esp, 8
    
    push dword [c]
    push dword [b]
    push dword [a]
    push dword [n]
    call hanoi
    add esp, 16
    
    xor eax, eax
    ret
hanoi:
    push ebp
    mov ebp, esp
    
    mov eax, dword[ARG1]
    cmp eax, 0
    je .L1
    
    dec eax
    push eax
    
    push dword[ARG2]
    push dword[ARG4]
    push dword[ARG3]
    push eax
    call hanoi    
    add esp, 16
    
    push dword[ARG3]
    push dword[ARG2]
    push fmt
    call printf
    add esp, 12
    
    pop eax
    push dword[ARG2]
    push dword[ARG3]
    push dword[ARG4]
    push eax
    call hanoi    
    add esp, 16

.L1:            
    mov esp, ebp
    pop ebp
    ret
