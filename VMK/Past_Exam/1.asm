extern qsort
extern printf

%assign SIZE_ITEM 8
%assign COUNT_OF_ELEMENTS 4

section .bss
	np resd 8
	
section .rodata
	fmt db `%d\n`, 0
	fmtl db `%d `, 0
	
section .data
	arr dd 20, 25, 5, 10, 6, 8, 1, 2
	len dd COUNT_OF_ELEMENTS
	
section .text
	global main
main:
	; Sort
	push compare
	push SIZE_ITEM
	push COUNT_OF_ELEMENTS
	push arr
	call qsort
	add esp, 16
	
	
	; Начальное значение np
	mov eax, dword[arr]
	mov dword[np], eax
	mov eax, dword[arr + 4]
	mov dword[np + 4], eax
	
	xor ebx, ebx		; кол-во новых сегментов
	mov esi, 1			; счетчик
.loop:
	cmp esi, dword[len]		; если прошли весь массив!
	je .print
	
	mov eax, dword[np + 8 * ebx + 4]	; берем второй элемент
	mov ecx, dword[arr + 8 * esi] 		; следующий сегмент(левый)
	cmp eax, ecx
	jl .L1
	; сравниваем во втором сегменте элемент
	mov ecx, dword[arr + 8 * esi + 4]
	cmp eax, ecx
	jge .endl			; то есть старый сегмент полностью вмещает в себя все!
	
	; Изменяем пределы нового сегмента!
	mov dword[np + 8 * ebx + 4], ecx
	jmp .endl
	
.L1:
	; Создаем новый сегмент в новом массиве
	inc ebx
	mov dword[np + 8 * ebx], ecx
	mov eax, dword[arr + 8 * esi + 4]
	mov dword[np + 8 * ebx + 4], eax
	jmp .endl
	
.endl:
	inc esi
	jmp .loop
	
	
	dec ebx
.print:
	cmp ebx, 0		; кол-во тех сегментов в новом массиве
	jl .exit
	
	lea eax, [np + 8 * ebx]		; будем выводить в обратном порядке
	push eax
	push dword[eax]
	push fmtl
	call printf
	add esp, 8
	
	pop eax
	push dword[eax + 4]
	push fmt
	call printf
	add esp, 8
	
	dec ebx
	jmp .print
	
.exit:
	xor eax, eax
	ret
	
compare:
	push ebp
	mov ebp, esp
	push ebx
	push esi
	
	mov esi, dword[ebp + 8]
	mov ebx, dword[ebp + 12]
	
	mov esi, dword[esi]
	mov ebx, dword[ebx]
	
	cmp esi, ebx
	jg .L1
	jl .L2
	
; 	if equal
	xor eax, eax
	jmp .end
	
.L1:
	mov eax, 1
	jmp .end
	
.L2:
	mov eax, -1
	jmp .end
	
.end:
	pop esi
	pop ebx
	mov esp, ebp
	pop ebp
	ret
