#ifndef STDIO
	#include <stdio.h>
#endif
#ifndef STDLIB
	#include <stdlib.h>
#endif

typedef struct node {
	int data;
	struct *left, *right;
} node;

node **init() {
	node **tmp = malloc(sizeof(node *));
	return tmp;
}

void rotate_right() {
    
}

void rotate_left() {
    
}

void push(node **root, int data) {
	if (isEmpty(root)) {
		node *tmp = malloc(sizeof(node));
		if (tmp) {
			*root = tmp;
			tmp->data = data;
		} else {
			fprintf(stderr, "Sorry, you havn't got any memory for it\n");
		}
		return;
	}
	
	/* Перемещаемся по дереву в поиске подходящего места для data */
	node *tmp = *root;
	node *prev;
	while (tmp->left || tmp->right) {
		int tree_data = tmp->data;
		prev = tmp;
		if (data > tree_data) {
			tmp = tmp->right;
		} else if (data < tree_data) {
			tmp = tmp->left;
		} else {
			return;
		}
	}
	
	/* Создаем нового ребенка дерева + с помощью prev узнаем родителя для этого ребенка и соединяем*/
	node *new_block = malloc(sizeof(node));
	if (new_block) {
		new_block->data = data;
		
		if (prev->data > data) {
			prev->left = new_block;
		} else {
			prev->right = new_block;
		}
	} else {
		fprintf(stderr, "Sorry, you haven't got any memory for new data\n");
	}
	
	/* Теперь нужно отбалансировать это дерево! */
}

void pop(node **root, int data) {
	
}

int isEmpty(node **root) {
	if (root) {
		return 0;
	} else {
		return 1;
	}
}


int main(void) {
	node **tree = NULL;
    
	tree = init();
	return 0;
}
