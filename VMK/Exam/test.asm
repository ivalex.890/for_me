extern printf

section .data
	fmt db "Hello world", 10, 0
section .text
	global main
main:
	push ebp
	mov ebp, esp
	and esp, ~0xff		; нам он это не говорил
	
	push fmt
	call printf
	add esp, 4
	
	xor eax, eax
	mov esp, ebp
	pop ebp
	ret
