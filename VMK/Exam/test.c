typedef struct a {
	int data;
	struct a *next;
} a;

int length(a *p) {
	return p? length(p->next) + 1: 0;
}

int main() {
	length();
}

/*
 * Внутри функции cdecl можно вызывать
 * функции stdcall __attribute__((stdcall)) сама функция
 */
