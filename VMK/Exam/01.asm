; базовый адрес - 0x00 40 10 d0

section .data
	wow dw 0x1234, 0xbeaf ; 0x00 40 10 d0
	tasty dd beaf			; 0x00 40 10 d4

section .bss
	beaf resd 1				; 0x00 40 10 d8
	
section .text
	global main
	
main:
	movsx eax, byte [wow + 3] 	; (1) eax = ff ff ff 9d
	sub ax, 0x4321				; (2) eax = ff ff bc 9d
	xchg ah, byte [tasty + 1]	; (3) eax = ff ff 10 9d
								; флаги после (2) 
								; zf, cf, of, sf
								; 0    1  0    1
								
	xor eax, eax
	ret
