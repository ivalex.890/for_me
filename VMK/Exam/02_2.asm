section .data
	

section .text
	global main 
main:
	push ebp
	mov ebp, esp
	and esp, ~0xf
	
	mov eax, dword[p]
	cmp eax, 0
	je .L1
	
	mov eax, dword[q]
	cmp eax, 0
	je .L1
	
	mov eax, dword[q]
	lea ecx, [eax]
	add ecx, 2
	lea edx, ecx
	mov dword[q], edx

	movzx eax, word[eax]
	mov edx, dword[p]
	add edx, 4
	mov	ecx, dword[edx]		; p[2]
	movzx edx, ax 		; q
	mov	eax, dword[p]	; p
	add	eax, 4				; p[2]
	add	edx, ecx		; +
	mov dword[eax], edx ; =
	mov	eax, dword [eax] ; += ЗНАЧЕНИЕ
	
	
.L1:
	push eax
	push eax
	push fmt
	call printf
	add esp, 8
	
	pop eax
	ret
