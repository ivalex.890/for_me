section .data
	p dd 1, 2, 3, 4
	q dw 2, 1, 3
	
section .text
	global main
main:
; p && q && p[2] += *q--; eax = 1
	mov eax, dword[p]
	cmp eax, 0
	jne .L1
	
	mov eax, dword[q]
	cmp eax, 0
	jne .L1
	
	; q - 
	movsx ebx, word [q]		; *q
	lea eax, [p + 8]		; p[2]
	
	add dword[eax], ebx		; +=
	and dword[eax], q
	
	jmp .L2
.L1:
	mov eax, 1
.L2:
	ret 
	
