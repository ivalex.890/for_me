extern printf

section .data
	fmt db `%x\n`, 0
	
section .text
	global main
main:
	mov ax, 0x0160
	add ax, 0xaaaa
	pushf
	pop eax
	push eax
	push fmt
	call printf
	add esp, 8
	
.end:
	xor eax, eax
	ret
