struct seq {
	short s;
	signed char c;
	struct seq *next;
};
// typedef struct seq seq;
// 
// extern int f(struct seq *q);
int f(struct seq *q) {
	if (!q) {
		return 42;
	}
	return (q->s + f(q->next)) % q->c;
}

// int main() {
// 	seq *n3 = NULL;
// 	seq n1 = {1, 2, n3};
// 	
// 	printf("%d", f(&n1));
// 	
// 	return 0;
// }
