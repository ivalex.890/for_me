extern printf

section .data
	fmt db "%d %d", 10, 0
	
section .text
	global main
main:
	rdtsc	; считываем кол-во тактов с регистра TLS в EDX:EAX
	push eax
	push edx
	push fmt
	call printf
	add esp, 12
	
	xor eax, eax
	ret
	
