
section .text
	global f
f:
	push ebp
	mov ebp, esp
	and esp, ~0xf
	push ebx
	
	mov eax, dword[ebp + 8]
	cmp dword[eax], 0
	je .L1
	
	movsx ebx, word[eax]
	
	push eax
	push dword[eax + 4]
	call f
	add esp, 4
	
	add ebx, eax
	pop eax
	movsx cx, byte[eax + 2]
	movsx ecx, cx
	
	xor edx, edx
	mov eax, ebx
	idiv ecx
	mov eax, edx	; вроде тут остаток
	jmp .end
	
.L1:
	mov eax, 42
.end:	
	
	pop ebx
	mov esp, ebp
	pop ebp
	ret
