#!/bin/bash

# Готовим созданную библиотеку для использования
LIB_C="./Sorts/lib.c"
LIB_O="./Sorts/lib.o"
gcc -c -Wall -g -o $LIB_O $LIB_C

# Файл .c (и иполняемый) файл, которые генерирует тесты для сортировок
TESTS_C="./Sorts/make_tests.c"
TESTS_C_="./Sorts/make_tests.out"
gcc -Wall -g -o $TESTS_C_ $TESTS_C
./$TESTS_C_

# Файл, который сортирует тесты с помощью алогритма прямого выбора
SELECTION_SORT_TESTS="./Sorts/selection_tests.c"
SELECTION_SORT_TESTS_O="./Sorts/selection_tests.o"
NAME_3="./Sorts/c.out"
gcc -c -Wall -g -o $SELECTION_SORT_TESTS_O $SELECTION_SORT_TESTS
gcc -Wall -g -o $NAME_3 $SELECTION_SORT_TESTS_O $LIB_O
./$NAME_3

# Файл, который сортирует тесты с помощью быстрой сортировки
QUICK_SORT_TESTS="./Sorts/quick_tests.c"
QUICK_SORT_TESTS_O="./Sorts/quick_tests.o"
NAME_4="./Sorts/d.out"
gcc -c -Wall -g -o $QUICK_SORT_TESTS_O $QUICK_SORT_TESTS
gcc -Wall -g -o $NAME_4 $QUICK_SORT_TESTS_O $LIB_O
./$NAME_4

# Файл, который проверяет на ошибки алогритмов сортировки 
# сгенерированных тестов 
CHECK_TESTS="./Sorts/check_test.c"
CHECK_TESTS_="./Sorts/check_test.out"
gcc -Wall -g -o $CHECK_TESTS_ $CHECK_TESTS
./$CHECK_TESTS_

#Выводим статистику кол-ва перемещений(обменов) и сравнений
SELECTION_SORT="./Sorts/selection_sort.c"
SELECTION_SORT_O="./Sorts/selection_sort.o"

QUICK_SORT="./Sorts/quick_sort.c"
QUICK_SORT_O="./Sorts/quick_sort.o"

NAME_1="./Sorts/a.out"
NAME_2="./Sorts/b.out"

gcc -c -Wall -g -o $SELECTION_SORT_O $SELECTION_SORT
gcc -Wall -g -o $NAME_1 $SELECTION_SORT_O $LIB_O
gcc -c -Wall -g -o $QUICK_SORT_O $QUICK_SORT
gcc -Wall -g -o $NAME_2 $QUICK_SORT_O $LIB_O

echo "It's statics for Selection sort\n"
./$NAME_1
echo "It's statics for Quick sort\n"
./$NAME_2

# Удаляем все те файлы, которые создаются во время исполнения программы
rm ./Sorts/*.out
rm ./Sorts/*.o
rm ./Sorts/tests/*
