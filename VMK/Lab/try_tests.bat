:: Программа, выполняющая на Windows.
:: Проходим лишь все 10/100/1000/10000 последовательности(без созданных тестов в папке tests).
@echo off
cls
title Try tests in lab(VMK)

cd .\Sorts\
:: Готовим созданную библиотеку для использования
SET LIB_C=lib.c
SET LIB_O=lib.o
gcc -c -Wall -g -o %LIB_O% %LIB_C%

:: Выводим статистику кол-ва перемещений(обменов) и сравнений
SET SELECTION_SORT=selection_sort.c
SET SELECTION_SORT_O=selection_sort.o

SET QUICK_SORT=quick_sort.c
SET QUICK_SORT_O=quick_sort.o

SET NAME_1=a.out
SET NAME_2=b.out

gcc -c -Wall -g -o %SELECTION_SORT_O% %SELECTION_SORT%
gcc -Wall -g -o %NAME_1% %SELECTION_SORT_O% %LIB_O%
gcc -c -Wall -g -o %QUICK_SORT_O% %QUICK_SORT%
gcc -Wall -g -o %NAME_2% %QUICK_SORT_O% %LIB_O%

echo "It's statics for Selection sort\n"
%NAME_1%
echo "It's statics for Quick sort\n"
%NAME_2%

:: Удаляем все те файлы, которые создаются во время исполнения программы
@del /f .*.out
@del /f .\*.o

@pause
