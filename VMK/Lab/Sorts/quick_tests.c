/* Программа прогоняющая по 5 (вторым) тестам
 * алогритм быстрой сортировки.
 * То есть:
 * 1) Cчитывает из testN последовательность элементов.
 * 2) Cортирует элементы последовательности. 
 * 3) Записывает отсортированную посл-ть в testN.
*/

#include <stdio.h>
#include <stdlib.h>
#include "lib.h"

typedef long long int ll;

int main(void) {
	char filename[80];
	for (int i = 5; i < CNT_TESTS; i++) {
		sprintf(filename, "./Sorts/tests/test%d.bin", i);
		
		FILE *file = fopen(filename, "wb+");
		if (file == NULL) {
			/* Если алогритм сортировки оказался не коректным */
			fprintf(stderr, "It's null pointer to %s. \
			Error in %s\n", filename, __FILE__);
			break;
		}
		
		ll *arr = malloc(DIM_SEQ * sizeof(ll));
		for (int k = 0; k < DIM_SEQ; k++) {
			fread(&(arr[k]), sizeof(ll), 1, file);
		}
		
		quick_sort(DIM_SEQ, arr);
		
		fseek(file, 0, SEEK_SET);
		for (int k = 0; k < DIM_SEQ; k++) {
			fwrite(&(arr[k]), sizeof(ll), 1, file);
		}
		fclose(file);
	}
	
	return 0;
}
