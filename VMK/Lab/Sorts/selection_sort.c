#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "lib.h"

#define COUNT 4	/* Кол-во подданных массивов на сортировку */

/*  Для удобства обозначения
    типа переменной long long int
    было выбраноназвание ll */
typedef long long int ll;

int main(void) {
	srand(time(NULL));

	/* Сортируем массивы, состоящии из 10|100|1000|10000 элементов */
	int n = 10;
	for (int i = 0; i < COUNT; i++) {
		printf("\nStatic for %d-th array\n", n);
		print_line();
        
		ll *arr = malloc(n * sizeof(ll));
		if (arr == NULL) {
			fprintf(stderr, "Error with memory\n");
			break;
		}

		/* I) Прямой порядок (Лучший случай) */
		for (int j = 0; j < n; j++) {			
			arr[j] = (ll) rand() * rand() * rand() * rand() * rand() ;
		}
		selection_sort(n, arr);
		printf("I) The best situation:\n");
		selection_sort_wst(n, arr);

		print_line();
        
		/* II) Обратный порядок (Худший случай) */
		for (int j = 0; j < n; j++) {			
			arr[j] = (ll) rand() * rand() * rand() * rand() * rand() ;
		}
		rev_selection_sort(n, arr);
		printf("II) The worth situation:\n");
		selection_sort_wst(n, arr);

		print_line();
		printf("\n");
        
		/* III и IV) Случайный порядок */
		/* На каждый из случаев запускаем по 4 теста
		   и берем средний рез-ат из них */
		for (int cnt = 0; cnt < 2; cnt++) {
			if (cnt == 0) {
				printf("III) The random versions:\n");
			} else {
				printf("IV) The random versions:\n");
			}
			for (int m = 0; m < 4; m++) {
				for (int j = 0; j < n; j++) {			
					arr[j] = (ll) rand() * rand() * rand() * rand() * rand() ;
				}
				selection_sort_wst(n, arr);
			}
			print_line();
			printf("\n");
		}
		printf("\n");

		n *= 10;
		free(arr);
	}

	return 0;
}
