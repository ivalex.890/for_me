/* Программа, проверяющая рез-ты сортировки. 
 * Это вторая проверка в сортировке. Первая всегда
 * проходит до записи в testN.
*/

#include <stdlib.h>
#include <stdio.h>
#include "lib.h"

typedef long long int ll;

int main(void) {
	char filename[80];
	for (int i = 0; i < CNT_TESTS; i++) {
		sprintf(filename, "./Sorts/tests/test%d", i);
		FILE *in = fopen(filename, "wb+");
		
		if (in == NULL) {
			fprintf(stderr, "Sorry, in %s - null pointer.\
			Error in %s\n", filename, __FILE__);
			break;
		}
		
		ll a, b;
		char flag = 1;
		fread(&a, sizeof(ll), 1, in);
		for (int j = 0; j < DIM_SEQ - 2; j++) {
			fread(&b, sizeof(ll), 1, in);
			
			if (llabs(a) > llabs(b)) {
				fprintf(stderr, "Sorry, it's wrong..\n");
				flag = 0;
				break;
			}
			a = b;
		}
		
		if (flag) {
			printf("%d Test: All right!\n", i);
		}
		
		fclose(in);
	}
	
	return 0;
}
