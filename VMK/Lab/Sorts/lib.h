#define CNT_TESTS 10
#define DIM_SEQ 10000

void print_arr(int n, long long int *arr);
int check_sort(int n, long long int *arr);
void print_line(void);

void selection_sort(int n, long long int *arr);
void rev_selection_sort(int n, long long int *arr);
void selection_sort_wst(int n, long long int *arr);

void quick_sort(int n, long long int *arr);
void rev_quick_sort(int n, long long *arr);
void quick_sort_wst(int n, long long int *arr);


