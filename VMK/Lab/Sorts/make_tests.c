/* Программа, моделирующая тесты для сортировок
 * Первые 5 тестов для 
 * Вторые 5 тестов для Quick Sort
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "lib.h"

typedef long long int ll;

int main(void) {
	srand(time(NULL));
    
	char filename[80];
	for (int i = 0; i < CNT_TESTS; i++) {
		sprintf(filename, "./Sorts/tests/test%d.bin", i);
        
		FILE *out = fopen(filename, "wb");
		if (out == NULL) {
			fprintf(stderr, "Can't make %s (file).\
			Error in %s\n", filename, __FILE__);
		}
		
		/* Генерируем случайныке последовательности для тестов */
		for (int j = 0; j < DIM_SEQ; j++) {
			ll tmp = (ll) rand() * rand() * rand() * rand() * rand();
			fwrite(&tmp, sizeof(ll), 1, out);
		}

		fclose(out);
	}

	return 0;
}
