#ifndef STDIO
	#include <stdio.h>
#endif
#ifndef STDLIB
	#include <stdlib.h>
#endif

#include "./lib.h"

/*  Для удобства обозначения
 * типа переменной long long int
 * было выбраноназвание ll */
typedef long long int ll;

/* Общие функции для отладки и проверки алогритмов сортировки */
void print_arr(int n, ll *arr) {
	for (int i = 0; i < n; i++) {
		printf("%lld\n", arr[i]);
	}
	printf("\n");
}

int check_sort(int n, ll *arr) {
	/* flag:
		* 0 - последовательность элементов отсортирована верно.
		* 1 - последовательность элементов отсортирована не верно.
	*/
	int flag = 0;
	for (int i = 0; i < n - 1; i++) {
		if (llabs(arr[i]) > llabs(arr[i + 1])) {
			//printf("%lld %lld\n", arr[i], arr[i+1]);
			return (!flag);
		}
	}
	
	return flag;
}

/* Требуется для улучшения качетсва просмотра статистики */
void print_line(void) {
	for (int i = 0; i < 25; i++) {
		printf("-");
	}
	printf("\n");
}

/* Сортировка прямым выбором */
void selection_sort(int n, ll *arr) {
	for (int i = 0; i < n - 1; i++) {
		int min = i;
		for (int j = i + 1; j < n; j++) {
			if (llabs(arr[j]) < llabs(arr[min])) {
				min = j;
			}
		}
		
		if (min != i) {
			ll tmp = arr[min];
			arr[min] = arr[i];
			arr[i] = tmp;
		}
	}
}

/* selection_sort_with_statics.
 * Сортировка со статистикой кол-ва
 * сравнений и обменов.
*/
void selection_sort_wst(int n, ll *arr) {
	ll cmp = 0;
	ll swaps = 0;
	for (int i = 0; i < n - 1; i++) {
		int min = i;
		for (int j = i + 1; j < n; j++) {
			if (llabs(arr[j]) < llabs(arr[min])) {
				min = j;
			}
			cmp++;
		}
		
		if (min != i) {
			ll tmp = arr[min];
			arr[min] = arr[i];
			arr[i] = tmp;
			swaps++;
		}
	}
	
	printf("%lld %lld\n", swaps, cmp);
}

/* Сортировка прямым выбором, лишь с обратным порядком*/
void rev_selection_sort(int n, ll *arr) {
	for (int i = 0; i < n - 1; i++) {
		int max = i;
		for (int j = i + 1; j < n; j++) {
			if (llabs(arr[j]) > llabs(arr[max])) {
				max = j;
			}
		}
		
		if (max != i) {
			ll tmp = arr[max];
			arr[max] = arr[i];
			arr[i] = tmp;
		}
	}
}

/* 	Функции относящиеся к быстрой сортировке без вывода
	статистики кол-ва сравнений и обменов */ 

void sort(int l, int r, ll *arr) {
	if (l < r) { 
		ll pointer = llabs(arr[(l + r)/2]);
		int left = l;
		int right = r;
		do {
			while (llabs(arr[left]) < pointer) {
				left++;
			}
			while (llabs(arr[right]) > pointer) {
				right--;
			}
			if (left <= right) {
				ll tmp = arr[left];
				arr[left] = arr[right];
				arr[right] = tmp;
        
				right--;
				left++;
			}
		} while (left < right);
		sort(l, right, arr);
		sort(left, r, arr);
	}
}

/* 	Быстрая сортировка, которую можно использовать
	Вход:
		n - кол-во элементов последовательности,
		*arr - "указатель" на начало посл-ти. 
	Вывод:
		- отсортированный массив */
void quick_sort(int n, ll *arr) {
	int left = 0;
	int right = n;
	sort(left, right, arr);
}

/* Функция быстрой сортировки в обратную "cторону"
*/
void sort_rev(int l, int r, ll *arr) {
	if (l < r) { 
		ll pointer = llabs(arr[(l + r)/2]);
		int left = l;
		int right = r;
		do {
			while (llabs(arr[left]) > pointer) {
				left++;
			}
			while (llabs(arr[right]) < pointer) {
				right--;
			}
			if (left <= right) {
				ll tmp = arr[left];
				arr[left] = arr[right];
				arr[right] = tmp;
        
				right--;
				left++;
			}
		} while (left < right);
		sort(l, right, arr);
		sort(left, r, arr);
	}
}

void rev_quick_sort(int n, ll *arr) {
	int left = 0;
	int right = n;
	sort_rev(left, right, arr);
}

/* Функции, представляющие ту же быструю сортировку,
 * но только с выводом статистики (кол-ва обменов и сравнений.
*/
void sort_wst(int l, int r, ll *arr, ll *swaps, ll *cmp) {
	if (l < r) { 
		ll pointer = llabs(arr[(l + r)/2]);
		int left = l;
		int right = r;
		do {
			while (llabs(arr[left]) < pointer) {
				left++;
				(*cmp)++;
			}
			while (llabs(arr[right]) > pointer) {
				right--;
				(*cmp)++;
			}
			if (left <= right) {
				ll tmp = arr[left];
				arr[left] = arr[right];
				arr[right] = tmp;
        
				right--;
				left++;
				(*swaps)++;
				(*cmp)++;
			}
		} while (left < right);
		sort_wst(l, right, arr, swaps, cmp);
		sort_wst(left, r, arr, swaps, cmp);
	}
}

/* Быстрая сортировка со статистикой кол-ва 
 * обменов и сравнений.
 *  quick_sort_with_statics
*/
void quick_sort_wst(int n, ll *arr) {
	/* Начальные константы для входного массива*/
	int left = 0;
	int right = n;
	ll swaps = 0;
	ll cmp = 0;
	sort_wst(left, right, arr, &swaps, &cmp);
    
	printf("%lld %lld\n", swaps, cmp);
}

