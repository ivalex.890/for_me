/* Программа, прогоняющая первые 5 тестов по алгоритму
 * То есть 
 * 1) Cчитывает из testN последовательность элементов.
 * 2) Cортирует элементы последовательности. 
 * 3) Записывает отсортированную посл-ть в testN.
*/

#include <stdio.h>
#include <stdlib.h>
#include "lib.h"

typedef long long int ll;

int main(void) {
	char filename[80];
	for (int i = 0; i < CNT_TESTS/2; i++) {
		sprintf(filename, "./Sorts/tests/test%d.bin", i);
		
		FILE *file = fopen(filename, "wb+");
		if (file == NULL) {
			fprintf(stderr, "It's null pointer to %s. \
			Error in %s\n", filename, __FILE__);
			break;
		}
		
		ll *arr = malloc(DIM_SEQ * sizeof(ll));
		for (int k = 0; k < DIM_SEQ; k++) {
			fread(&(arr[k]), sizeof(ll), 1, file);
		}
		
		selection_sort(DIM_SEQ, arr);
		if (check_sort(DIM_SEQ, arr)) {
			/* Если алогритм оказался не некоректным */
			fprintf(stderr, "Sorry, my algotithm it's not right..\n");
		}
		
		fseek(file, 0, SEEK_SET);
		for (int k = 0; k < DIM_SEQ; k++) {
			fwrite(&(arr[k]), sizeof(ll), 1, file);
		}
		fclose(file);
	}
	
	return 0;
}
