#include <stdio.h>
#include "countof.h"


int main(void) {
	char a[81], b[81];

	int na, nb;

	scanf("%s %s", a, b);

	na = countof(a, '$'); nb = countof(b, '$');

	printf("%s\n", na > nb ? a : b);

	return 0;
}
