#include <stdio.h>
#include <stdlib.h>

int lenstr(char *str) {
	int len = 0;
	int i = 0;
	while (str[i] != '\0') {
		len++;
		i++;
	}
	
	return len;
}

int main(int argc, char *argv[]) {
	int n = 5;
	char *string = calloc(n, sizeof(char));
    
	int cnt = 0;
	for (int i = 1; i < argc; i++) {
		int len = lenstr(argv[i]);
		if (cnt + len >= n) {
			n *= 2;
			string = realloc(string, n);
		}
		
		for (int k = 0; k < len; k++) {
			string[cnt++] = argv[i][k];
		}
	}
	
	for (int i = 0; i < cnt; i++) {
		printf("%c", string[i]);
	}
	
	return 0;
}
