#include <stdio.h>
#include <stdlib.h>

int lenstr(char *str) {
	int len = 1;
	while (str[len - 1] != '\0') {
		len++;
	}
	return len;
}

int main(int argc, char *argv[]) {
	if (argc == 1) {
		return 0;
	}
	
	int n = lenstr(argv[1]);
	char *string = calloc(n, sizeof(char));
    
	int cnt = 1;
	int len = lenstr(argv[2]);
	int m = 0;
	for (int i = 0; i < n; i++) {
		if (argv[1][i] == argv[2][0]) {
			for (int k = 1; k < len; k++) {
				if (argv[1][++i] == argv[2][k]) {
					cnt++;
				} else {
					break;
				}
			}
			
			cnt = 1;
		}
		
		string[m++] = argv[1][i];
	}
	
	for (int i = 0; i < n; i++) {
		if (string[i] != '\0') {
			printf("%c", string[i]);
		}
	}
	
	free(string);
    
	return 0;
}
