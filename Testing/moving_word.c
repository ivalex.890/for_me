/*
 * Данная программа позволяет выводить слово и перемещать с помощью
 * стролок клавиатуры
*/
#include <ncurses.h>
#include <string.h>
#include <stdlib.h>

#define LENGTH_PERSON 3
#define WIDTH_PERSON 5

#define NORMAL 1
#define RED 2
#define YELLOW 3
#define BLUE 4
#define GREEN 5

/*
 * 		#TODO
 * 		1) Можно ввести параметр - скорость (такую, чтобы она была примерно одной и той же для 
 *	как для оси x и y. А то полкчается, что по оси x летим, а по оси y мы еле двигаемся.
 *		2) Можно ввести панель с выбором стиля изначального перса
 * 		3) Можно ввести мобов с разукрашкой))
 * 
 * 		4) Можно ввести препятствия 
 * 
 */


typedef struct gamer {
	chtype **word;
} gamer;

gamer *create_pers(void) {
	/* Если игрок не захочет сменить перса, то базовый вот
	* 	 ___
		|@ @|
		|___|
	* 
   #  #
 #  #  #
#       #
#       #
 #     #
  #   #
   # #  
    #
	*/
    
	gamer *pers = calloc(1, sizeof(gamer));
	pers->word = calloc(LENGTH_PERSON, sizeof(chtype *));
    
	chtype symbols[LENGTH_PERSON][WIDTH_PERSON] =	{{' ', '_', '_', '_', ' '}, 
													{'|', '@', ' ', '@', '|'}, 
													{'|', '_', '_', '_', '|'}};
                            
	for (int i = 0; i < LENGTH_PERSON; i++) {
		pers->word[i] = calloc(WIDTH_PERSON, sizeof(char));
		for (int j = 0; j < WIDTH_PERSON; j++) {
			pers->word[i][j] = symbols[i][j] | A_BLINK | COLOR_PAIR(GREEN);
		}
	}
	
	return pers;
}

void print_pers(gamer *pers, int x, int y) {
	move(x, y);
    
	for (int i = 0; i < LENGTH_PERSON; i++) {
		for (int j = 0; j < WIDTH_PERSON; j++) {
			addch(pers->word[i][j]);
		}
		move(x + 1 + i, y);
	}

	move(x, y);
}

void hide_pers(gamer *pers, int x, int y) {
	int dy = WIDTH_PERSON;
	int dx = LENGTH_PERSON;
	for (int i = 0; i < dx; i++) {
		for(int j = 0; j < dy; j++) {
			printw(" ");
		}
		
		move(x + 1, y);
		x++;
	}
	
	x -= dx;
	move(x, y);
}

void hide_word(char word[], int x, int y) {

    
	move(x, y);
	
	int len = strlen(word);
	for (int i = 0; i < len; i++) {
		printw(" ");
	}
}
/*
 ___
| { |__
|_{_|
 ___
|\ /|
|___|

 ___
|] [|
|___|

 ___
|# #|
|___|

 ___
|@ @|
|___|

 ___
|! #|
|___|

 ___
|~ ~|
|___|

 ___
|+ +|
|___|

 ___
|$ $|
|___|
*/

void move_word(char word[], int x, int y) {
	move(x, y);
    
	int len = strlen(word);
	for (int i = 0; i < len; i++) {
		printw("%c", word[i]);
	}
	
	move(x, y);
}

int main(void) {
	/* Beginig settings for game*/
	initscr();
	keypad(stdscr, TRUE);
	noecho();
	curs_set(0);
    
	if (!has_colors()) {
		printw("Sorry, the colors incompetible with your system...");
		getch();
		endwin();
	}
	
	start_color();
	init_pair(RED, COLOR_RED, COLOR_BLACK);
	init_pair(NORMAL, COLOR_WHITE, COLOR_BLACK);
    init_pair(GREEN, COLOR_GREEN, COLOR_BLACK);
    
	int max_x = getmaxy(stdscr);
	int max_y = getmaxx(stdscr);
    
	int x = 0;
	int y = 0;
	move(x, y);
    
	/* Creating pers*/
	gamer *pers = create_pers();
	print_pers(pers, x, y);
    
	int c;
	while ((c = getch()) != 'q') {
		switch (c) {
			case KEY_LEFT:
				if (y - 1 >= 0) {
					hide_pers(pers, x, y);
					print_pers(pers, x, y - 1);
					y--;
				}
				break;
			case KEY_DOWN:
				if (x + LENGTH_PERSON < max_x) {
					hide_pers(pers, x, y);
					print_pers(pers, x + 1, y);
					x++;
				}
				break;
			case KEY_RIGHT:
				if (y + WIDTH_PERSON < max_y) {
					hide_pers(pers, x, y);
					print_pers(pers, x, y + 1);
					y++;
				}
				break;
			case KEY_UP: 
				if (x - 1 >= 0) {
					hide_pers(pers, x, y);
					print_pers(pers, x - 1, y);
					x--;
				}
				break;
			default:
				break;
		} 
	}
	
	endwin();
    
	return 0;
}
