#include <ncurses.h>

int main(void) {
	initscr();
	
	start_color();
	init_pair(1, COLOR_RED, COLOR_BLACK);
	
	int c = 'w' | COLOR_PAIR(1);
	printw("%c", c);
	getch();
    
	endwin();
}
