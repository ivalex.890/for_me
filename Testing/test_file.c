#include <stdio.h>

#define DIM 1

int main(void) {
	char a, b, c;
	scanf("%*c%c", &a);	// Означает пропустить все пробелы с данного места
	// Это значит, что первый символ будет считан, но не будет присвоен
    // * - модификатор подавления присваивания
	printf(" %c %c %c",  a);
	
    /*
	FILE *in;
	in = fopen("ll.txt", "w");

	char buf[DIM] = {}; 
	setbuf(in, buf);
    
	printf("%d\n", BUFSIZ);
	for (int i = 0; i < 5; i++) {
		char c = getchar();
		fprintf(in, "%c", c);
		fprintf(stdout, "%c", c);
		//fflush(in);
	}
	
	fclose(in);
     */   
	return 0;
}
