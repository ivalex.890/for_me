#include <stdio.h>

typedef long long int ll;

int main(void) {
	FILE *in = fopen("on", "wb+");
	ll arr[] = {-2, 3, -1, 3};
	for (int i = 0; i < 4; i++) {
		fwrite(&(arr[i]), sizeof(ll), 1, in);
	}
    
	fseek(in, 0, SEEK_SET);
	for (int i = 0; i < 4; i++) {
		ll a;
		fread(&a, sizeof(ll), 1, in);
		printf("%lld ", a);
	}
	
	return 0;
}
