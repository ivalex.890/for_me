// -----------------------------------
// c-style
#ifndef COUNT_H
#define COUNT_H

int countof(const char *s, char c);

#endif
// -----------------------------------

// -----------------------------------
// c++-style
// #pragma once
//
// int countof(const char *s, char c);
// -----------------------------------
