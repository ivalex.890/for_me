#include <stdio.h>
#include <stdlib.h>

#define LIMIT_FOR_BFACTOR 2

typedef struct node {
    int key;
    int data;
    struct node *left, *right, *parent;
    unsigned char height;
} node;

int bfactor(node *root) {
    node *left = root->left;
    node *right = root->right;
    
    return right->height - left->height;
}

void improve_height(node *root) {
    unsigned char height_l = (root->left)->height;
    unsigned char height_r = (root->right)->height;
    
    root->height = (height_l > height_r ? height_l: height_r) + 1;
    
    return;
}

void right_small_rotate(node *root) {
    node *left = root->left;
    node *tmp = left->right;
    
    root->left = tmp;
    left->right = root;
    
    // Должно быть изменена высота
    return;
}

void left_small_rotate(node *root) {
    node *right = root->right;
    node *tmp = right->left;
    
    root->right = tmp;
    right->left = root;
    
    // Долюны быть изменены высоты node.
    return;
}

void balance(node *root) {
    improve_height(root);
    
    if (bfactor(root) == LIMIT_FOR_BFACTOR) {
        
        if (bfactor(root->right) < 0) {
            right_small_rotate;
        }
        
        left_small_rotate;
        
    }else if (bfactor(root) == -LIMIT_FOR_BFACTOR) {
        
        if (bfactor(root->left) > 0) {
            left_small_rotate;
        }
        
        right_small_rotate;
    }
    
    return;
}

void add(node* root, int key, int data) {
    
    if (root->parent == NULL) {
        root->key = key;
        root->data = data;
        root->height = 0;
    }else {
        node *tmp = root;
        
        while (tmp != NULL) {
            
            if (tmp->key > key) {
                tmp->left->parent = tmp;
                tmp = tmp->left;
                
            }else if (tmp->key < key) {
                tmp->right->parent = tmp;
                tmp = tmp->right;
                
            }else {
                // Если ключи совпадают, то меняем лишь значения
                tmp->data = data;
                return;
            }
            
        }
        
        tmp = (node *) calloc(1, sizeof(node));
        tmp->key = key;
        tmp->data = data;
        
        // Нужен добавить в дерево и балансировать его
        while (tmp != root) {
            tmp = tmp->parent;
            balance(tmp);
        }
    }
    
    return;
}

void delete(node* root, int key) {
    
    if (!root) {
        return;
    }else {
        node *tmp = root;
        node *prev = root;
        
        // поиск нужного ключа
        while (1) {
        
            if (tmp->key == key) {
                break;
            }else if (tmp->key > key) {
                prev = tmp;
                tmp = tmp->left;
            }else {
                prev = tmp;
                tmp = tmp->right;
            }
        
        }
    
        // Нужно теперь правильно удалить
        
        if (!tmp->left && !tmp->right) {
            
            // Листок дерева
            if (prev->left == tmp) {
                prev->left = NULL;
            }else {
                prev->right = NULL;
            }
            
            free(tmp);
        }else if (!tmp->right) {
            // Если в 
            
        }else {
            // Что делать, если справа есть элементы
        }
    }
    
    return;
}

void search(node* root, int key) {
    node *tmp = root;
    
    while (tmp->key != key) {
        
        if (tmp->key > key) {
            tmp = tmp->left;
        }else {
            tmp = tmp->right;
        }
    }
    
    printf("%d ", tmp->data);
    
    return;
}

int main(void) {
    char c;
    
    int key;
    int data;
    node *head = (node *) calloc(1, sizeof(node));
    
    
    c = getchar();
    while (c != 'F') {
        
        if (c == 'A') {
            // добавление элементиа с ключом 
            scanf("%d", &key);
            scanf("%d", &data);
            
            add(head, key, data);
        }else if (c == 'S') {
            // поиск элемента по ключу
            scanf("%d", &key);
            
            search(head, key);
        }else if (c == 'D') {
            // удаление элемента
            scanf("%d", &key);
            
            delete(head, key);
        }
        
        c = getchar();
    }
    
    return 0;
}
