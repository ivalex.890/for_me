#include <stdio.h>
#include <stdlib.h>

int max(int a, int b) {
    return (a > b) ? a : b;
}

int main() {
    int n;
    int a;
    
    int sum;
    int best;
    
    scanf("%d", &n);
        
    sum = 0; best = 0;
    for (int i = 0; i < n; i++) {
        scanf("%d", &a);
            
        sum = max(a, sum + a);
        best = max(best, sum);
    }
        
    printf("%d\n", best);
    
    return 0;
}
