#include <stdio.h>
#include <stdlib.h>
/*
int *merge_two_list(int arr1[], int arr2[], int len1, int len2) {
	int *arr = calloc(len1 + len2, sizeof(int));
	
	int i = 0, j = 0;
	int cnt = 0;
	//printf("%d %d", len1, len2);
	while (i < len1 && j < len2) {
		if (arr1[i] > arr2[j]) {
			arr[cnt++] = arr2[j++];
		} else { 
			arr[cnt++] = arr1[i++];
		}
	}
	for(int m = 0; m < cnt; m++) {
		printf("arr[m] - %d\n", arr[m]);
	}
	printf("\n\n");
    /*
	if (i < len1) {
		for (int k = i; k < len1; k++) {
			arr[cnt++] = arr1[k];
		}
	} else if (j < len2) {
		for (int k = j; k < len2; k++) {
			arr[cnt++] = arr2[k];
		}
	}
	/*
	return arr;
    */

void sort(int arr1[], int buffer[], int l, int r) {
	if (l < r) {
		int m = (r + l) / 2;
		sort(arr1, buffer, l, m);
		sort(arr1, buffer, m + 1, r);
		
		int k = l; 
		for (int i = l, j = m + 1; i <= m && j <= r;) {
			if (arr1[i] < arr1[j])) {
				buffer[k++] = arr1[i++];
			} else {
				buffer[k++] = arr1[j++];
			}
		}
		
		if (i < m) {
			for (int d = i; d <= m; d++) {
				buffer[k++] = arr1[d];
			}
		} else if (j < r) {
			for (int d = j; d <= r; d++) {
				buffer[k++] = arr1[d];
			}
		}
		
		for (int i = l; i <= r; i++) {
			arr1[i] = buffer[i];
		}
		
	}
	
}

int main(void) {
	int arr1[] = {5, 8, -1, 0, -10};
	int buffer[5] = {};
    
	int len1 = 5;
	sort(arr1, buffer, 0, len1 - 1);
    
	for (int i = 0; i < 5; i++) {
		printf("%d\n", arr1[i]);
	}
	return 0;
}
