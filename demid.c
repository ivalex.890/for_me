#include <stdio.h>
#include <stdlib.h>

void get_x(int n, long long int matrix[][n], long long int B[]) { // получаю ответы
    long long int ans[n];
    
    long long int b;
    
    int i, j;
    for (i = n - 1; i >= 0; i--) {
        b = B[i];
        for (j = n - 1; j > i; j--) {
            b -= ans[j] * matrix[i][j];
        }
        if (matrix[i][j] != 0) {
            ans[i] = b / matrix[i][j];
        } else {
            ans[i] = 0; // может тут что не так? и чуть выше
        }
    }
    
    for (int i = 0; i < n; i++) {
        printf("%lld\n", ans[i]);
    }
}


void minus_rows(int col, int row, int n, long long int matrix[][n], long long int B[]) { // вычитаем строки
    if (row == n) {
        return;
    }
    
    long long int a1 = matrix[0][col], a2 = matrix[row][col];
    
    if (!a2) {
        return minus_rows(col, row + 1, n, matrix, B);
    }
    
    for (int i = col; i < n; i++) {
        matrix[0][i] *= a2;
        matrix[row][i] *= a1;
        matrix[row][i] -= matrix[0][i];
    }
    
    B[0] *= a2;
    B[row] *= a1;
    B[row] -= B[0];
    
    minus_rows(col, row + 1, n, matrix, B);
}


int main(void) {
    int n;
    scanf("%d", &n);
    
    long long int matrix[n][n], B[n];;
    
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            scanf("%lld", &matrix[i][j]);
        }
        scanf("%lld", &B[i]);
    }

    long long int tmp; // недопузырёк?
    for (int i = 0; i < n; i++) {
        if (!matrix[i][i]) {
            for (int j = i + 1; j < n; j++) {
                if (matrix[j][i]) {
                    for (int k = 0; k < n; k++) {
                        tmp = matrix[i][k];
                        matrix[i][k] = matrix[j][k];
                        matrix[j][k] = tmp;
                    }
                    tmp = B[i];
                    B[i] = B[j];
                    B[j] = tmp;
                    break;
                }
            }
        }
    }
    
    for (int i = 0; i < n; i++) { // запускаем основную функцию вычитания строк
        minus_rows(i, i + 1, n, matrix, B);
    }

    get_x(n, matrix, B); // ответ

  return 0;
  
}
