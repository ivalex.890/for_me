SECTION .text

GLOBAL countof
countof:
	PUSH EBP
	MOV EBP, ESP

	XOR EAX, EAX
	MOV CL, BYTE [EBP + 12]
	MOV EDX, DWORD [EBP + 8]
.l1:
	CMP BYTE [EDX], 0

	JE .l3

	CMP BYTE[EDX], CL
	JNE .l2

	INC EAX

.l2:
	INC EDX
	JMP .l1

.l3:
	leave
	ret
